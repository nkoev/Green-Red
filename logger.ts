export interface ILogger {
  log(...args: string[]): void;
}

export class ConsoleLogger implements ILogger {
  public log(...printables: string[]): void {
    console.log(...printables);
  }
}
