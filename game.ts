import { IBoard } from "./board";
import { ILogger } from "./logger";

export class Game {
  private result: number = 0;

  constructor(private board: IBoard, private logger: ILogger) {}

  public run(targetX: number, targetY: number, rounds: number): string {
    this.result = this.board.getCellColor(targetX, targetY);

    while (rounds > 0) {
      this.board.toNextGeneration();
      this.result += this.board.getCellColor(targetX, targetY);
      rounds--;
    }

    return this.result.toString();
  }

  public printResult(): void {
    this.logger.log(this.result.toString());
  }
}
