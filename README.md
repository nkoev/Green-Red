# Green Red Game

## DESCRIPTION

Green vs Red is a game played on a 2D grid that in theory can be infinite(in our case we will assme that x <= y< 1000).

Each cell on this grid can be either green(represented by 1) or red(represented by 0). The game always receives an initial state of the grid which we will call 'Generation Zero'. After that a set of 4 rules are applied across the grid and those rules form the next generation.
Rules that create the next generation:

1. Each red cell that is surrounded by exactly 3 or exactly 6 green cells will also become green in the next generation.
2. A red cell will stay red in the next generation if it has either 0, 1, 2, 4, 5, 7 or 8 green neighbours.
3. Each green cell surrounded by 0, 1, 4, 5, 7 or 8 green neighbours will become red in the next generation.
4. A green cell will stay green in the next generation if it has either 2, 3 or 6 green neighbours.

## ALGORITHM

### Inputs:
The algorithm accepts inputs as follows:

- Grid width,
- Grid height,
- Initial colors,
- Target cell coordinates (x, y),
- Game rounds

### Output:
When triggered the algorithm will print to the console the number of times target cell was green through game rounds.

## RUN

1. You need Node.js(v.12), npm(v.6) and git installed on your machine.
2. Clone repository ```git clone https://github.com/nkoev/Green-Red.git```
3. Move to project directory
4. TypeScript & ts-node required ```npm i typescript ts-node```
5. Run algorithm ```ts-node main.ts```
6. Change default inputs from main.ts file


