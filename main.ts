import { GreenRedBoard } from "./board";
import { ConsoleLogger } from "./logger";
import { Game } from "./game";

// Input Data
const boardWidth: number = 4;
const boardHeight: number = 4;
const boardColors: string[] = ["1001", "1111", "0100", "1010"];
const targetX: number = 2;
const targetY: number = 2;
const gameRounds: number = 15;

const playGame = (
  boardWidth: number,
  boardHeight: number,
  boardColors: string[],
  targetX: number,
  targetY: number,
  gameRounds: number
) => {
  const board = new GreenRedBoard(boardWidth, boardHeight, boardColors);
  const logger = new ConsoleLogger();
  const game = new Game(board, logger);

  game.run(targetX, targetY, gameRounds);
  game.printResult();
};

playGame(boardWidth, boardHeight, boardColors, targetX, targetY, gameRounds);
