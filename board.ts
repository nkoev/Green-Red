export interface IBoard {
  getCellColor(x: number, y: number): number;
  toNextGeneration(): number[][];
}

export class GreenRedBoard implements IBoard {
  private colors: number[][];

  constructor(private width: number, private height: number, colors: string[]) {
    this.colors = colors.map((line: string) => line.split("").map(Number));
  }

  public getCellColor(x: number, y: number): number {
    return this.colors[y][x];
  }

  public toNextGeneration(): number[][] {
    const nextGen: number[][] = [];

    for (let i = 0; i < this.height; i++) {
      nextGen.push([]);
      for (let j = 0; j < this.width; j++) {
        const cellColor = this.colors[i][j];
        const greenNeighbours = this.countGreenNeighbours(j, i);
        const nextCellColor = this.applyRules(cellColor, greenNeighbours);
        nextGen[i].push(nextCellColor);
      }
    }
    this.colors = nextGen;

    return nextGen;
  }

  private countGreenNeighbours(x: number, y: number): number {
    let greenNeighbours = 0;

    for (let i = -1; i <= 1; i++) {
      for (let j = -1; j <= 1; j++) {
        const x1 = x + i;
        const y1 = y + j;

        //Skip if outside board
        if (y1 < 0 || y1 >= this.height || x1 < 0 || x1 >= this.width) {
          continue;
        }

        //Skip given cell
        if (x1 === x && y1 === y) {
          continue;
        }
        greenNeighbours += this.colors[y1][x1];
      }
    }

    return greenNeighbours;
  }

  private applyRules(cellColor: number, greenNeighbours: number): number {
    return cellColor === 0 && [3, 6].includes(greenNeighbours)
      ? 1
      : cellColor === 1 && [0, 1, 4, 5, 7, 8].includes(greenNeighbours)
      ? 0
      : cellColor;
  }
}
